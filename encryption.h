#pragma once

#include <random>
#include <functional>
#include <algorithm>
#include <iostream>
#include <iterator>
#include <climits>
#include <string>
#include <sstream>
#include <iomanip>
#include <QByteArray>
#include <QString>

constexpr size_t num_rounds = 10;

QByteArray encrypt(const QByteArray& input, const QByteArray& key);
QByteArray decrypt(const QByteArray& input, const QByteArray& key);
