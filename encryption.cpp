#include "encryption.h"

#include <random>
#include <functional>
#include <algorithm>
#include <iterator>
#include <climits>

QByteArray encrypt(const QByteArray& input, const QByteArray& key)
{
    // Convert to bytes
    QByteArray data(key.size(), 0);
    std::copy(input.begin(), input.end(), data.begin());

    // Add padding
    {
        std::random_device r;
        std::seed_seq seed {r(), r(), r(), r(), r(), r(), r(), r()};
        auto rand = std::bind(std::uniform_int_distribution<>(0, UCHAR_MAX), std::mt19937(seed));
        data[input.size()] = 0;
        std::generate(data.begin() + input.size() + 1, data.end(), rand);
    }

    std::vector<int> random_key(num_rounds * data.size());
    std::seed_seq seed(key.begin(), key.end());
    auto rand = std::bind(std::uniform_int_distribution<>(0, UCHAR_MAX), std::mt19937(seed));
    std::generate(random_key.begin(), random_key.end(), rand);

    for (size_t r = 0; r < num_rounds; r++)
    {
        // Shift
        for (qsizetype i = 0; i < data.size(); i++)
        {
            data[i] += random_key[r * data.size() + i];
        }

        // Swap
        for (qsizetype i = 0; i < data.size(); i++)
        {
            int k = random_key[r * data.size() + i] % data.size();
            std::swap(data[i], data[k]);
        }
    }

    // Convert to hex
    return data.toHex();
}

QByteArray decrypt(const QByteArray& input, const QByteArray& key)
{
    // Convert to bytes
    QByteArray data = QByteArray::fromHex(input);

    std::vector<int> random_key(num_rounds * data.size());
    std::seed_seq seed(key.begin(), key.end());
    auto rand = std::bind(std::uniform_int_distribution<>(0, UCHAR_MAX), std::mt19937(seed));
    std::generate(random_key.begin(), random_key.end(), rand);

    for (int r = num_rounds - 1; r >= 0; r--)
    {
        // Swap
        for (int i = data.size() - 1; i >= 0; i--)
        {
            int k = random_key[r * data.size() + i] % data.size();
            std::swap(data[i], data[k]);
        }

        // Shift
        for (int i = data.size() - 1; i >= 0; i--)
        {
            data[i] -= random_key[r * data.size() + i];
        }
    }

    // Remove padding
    size_t message_size = key.size();
    for (size_t i = 0; i < message_size; i++)
    {
        if (data[i] == 0)
        {
            message_size = i;
            break;
        }
    }

    data.resize(message_size);

    return data;
}
