# Encryption
A simple (and terrible) encryption app made as an exercise.

## Build
The project is built with Qt. Use Qt Creator to compile the application.

## Usage examples
Encrypt `encryption_qt.exe in.txt out.txt` \
Decrypt `encryption_qt.exe -d in.txt out.txt`

## Contributors
Eivind Vold Aunebakk
