#include <QCoreApplication>
#include <QCommandLineParser>
#include <QFile>

#include "encryption.h"

QByteArray key = "8600c97773849254bf491e404fd23cd9b7f8037b8c55cc1215cc3c8f88f60e7e";

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QCoreApplication::setOrganizationName("eivaun");
    QCoreApplication::setApplicationName("FileEncrypter");
    QCoreApplication::setApplicationVersion("0.1");

    QCommandLineParser parser;
    parser.setApplicationDescription(QCoreApplication::applicationName());
    parser.addHelpOption();
    parser.addVersionOption();
    parser.addPositionalArgument("in", "The input file");
    parser.addPositionalArgument("out", "The output file");

    QCommandLineOption decryptOption("d", "Decrypt");
    parser.addOption(decryptOption);

    parser.process(a);
    bool decrypt_mode = parser.isSet(decryptOption);

    if(parser.positionalArguments().size() != 2) { parser.showHelp(1); }

    QString in_file_path = parser.positionalArguments().at(0);
    QString out_file_path = parser.positionalArguments().at(1);

    QFile in_file(in_file_path);
    if(!in_file.open(QFile::ReadOnly | QFile::Text))
    {
        qCritical() << "Unable to open file: " << in_file_path;
        return 1;
    }

    QByteArray message_data = in_file.readAll();
    in_file.close();

    auto transformed_message = decrypt_mode ? decrypt(message_data, key) : encrypt(message_data, key);

    QFile out_file(out_file_path);
    if(!out_file.open(QFile::WriteOnly | QFile::Text))
    {
        qCritical() << "Unable to open file: " << in_file_path;
        return 1;
    }
    out_file.write(transformed_message);
    out_file.close();

    return 0;
}
